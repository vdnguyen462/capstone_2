import { getProductInfor, onSuccess, renderProductList, showProductInfor } from "./controller.js";

const BASE_URL = 'https://63e21cc7109336b6cbffbdd3.mockapi.io/';

var productList = [];

let fetchProductList = () => {
    axios({
        url: `${BASE_URL}/QLSP`,
        method: "GET",
    }).then((res) => {
        productList = res.data;
        renderProductList(productList);
        })
        .catch((err) => {
        console.log(err);
        });
}
fetchProductList();

let addProduct = () => {
    axios({
        url: `${BASE_URL}/QLSP`,
        method: "POST",
        data: getProductInfor(),
    }).then((res) => {
        console.log(res);
        onSuccess("Thêm sản phẩm thành công");
        $('#exampleModalCenter').modal('hide');
        })
        .catch((err) => {
        console.log(err);
        });
}
window.addProduct = addProduct;

let deleteProduct = (idSP) => {
    axios({
        url: `${BASE_URL}/QLSP/${idSP}`,
        method: "DELETE",
    }).then((res) => {
        console.log(res);
        fetchProductList();
        onSuccess("Xoá sản phẩm thành công");
        })
        .catch((err) => {
        console.log(err);
        });
}
window.deleteProduct = deleteProduct;

let showProduct = (idSP) => {
    $("#exampleModalCenter").modal("show");
    axios({
        url: `${BASE_URL}/QLSP/${idSP}`,
        method: "GET",
    }).then((res) => {
        console.log(res.data);
        showProductInfor(res.data);
        })
        .catch((err) => {
        console.log(err);
        });
}
window.showProduct = showProduct;


let updateProduct = () => {
    var data = getProductInfor();
    axios({
        url: `${BASE_URL}/QLSP/${data.id}`,
        method: "PUT",
        data: data,
    }).then((res) => {
        fetchProductList();
        onSuccess("Cập nhật sản phẩm thành công")
        $('#exampleModalCenter').modal('hide');
    })
    .catch((err) => {
        console.log(err);
    });
}

window.updateProduct = updateProduct;
