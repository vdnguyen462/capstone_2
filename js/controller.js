export let renderProductList = (products) => {
    let contentHTML = "";
    products.forEach((item) => {
        let contentTr = `<tr>
                        <td>${item.id}</td>
                        <td>${item.name}</td>
                        <td>${item.price}</td>
                        <td>${item.image}</td>
                        <td>${item.moTa}</td>
                        <td>
                        <button onclick = "deleteProduct(${item.id})" class="btn btn-danger">Xoá</button>
                        <button onclick = "showProduct(${item.id})"
                        class="btn btn-primary">Xem</button>
                        </td>
                        </tr>`
        contentHTML+=contentTr;
    });
    document.getElementById("tbodyProduct").innerHTML = contentHTML;
}

export let getProductInfor = () => {
    let idSP = document.getElementById("txtID").value;
    let tenSP = document.getElementById("txtName").value;
    let giaSP = document.getElementById("txtPrice").value;
    let hinhSP = document.getElementById("txtHinhAnh").value;
    let moTa = document.getElementById("txtMoTa").value;
    return {
        id:idSP,
        name: tenSP,
        price: giaSP,
        image: hinhSP,
        moTa,
    }
}

export let showProductInfor = (item) => {
    document.getElementById("txtID").value = item.id;
    document.getElementById("txtName").value = item.name;
    document.getElementById("txtPrice").value = item.price;
    document.getElementById("txtHinhAnh").value = item.image;
    document.getElementById("txtMoTa").value = item.moTa;
}

export let onSuccess = (message) => {
    Toastify({
        text: message,
        className: "info",
        offset: {
            x: 50,
            y: 10,
        }
    }).showToast();
};